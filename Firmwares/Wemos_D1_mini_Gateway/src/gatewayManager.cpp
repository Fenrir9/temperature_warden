#include "gatewayManager.h"

void gatewayManager::setup_wifi() {
  delay(10);

  Serial.print("Connecting to: ");
  Serial.println(WIFI_SSID);

  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  randomSeed(micros());

  Serial.println("");
  Serial.print("WiFi connected, IP address: ");
  Serial.println(WiFi.localIP());
}

const char* gatewayManager::get_mqtttServer(){
  return mqttServer;
}

uint16_t gatewayManager::get_mqttPort(){
  return mqttPort;
}

const char* gatewayManager::get_mqtttUser(){
  return mqttUser;
}

const char* gatewayManager::get_mqttPassword(){
  return mqttPassword;
}
