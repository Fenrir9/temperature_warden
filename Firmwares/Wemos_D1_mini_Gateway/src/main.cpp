#include <Arduino.h>
#include "radioManager.h"
#include "gatewayManager.h"

radioManager radioM;
gatewayManager gatewayM;

#define SERIAL_BAUD   9600

//This part defines the LED pin and button pin
#define LED             2 //LED on D9
#define RX_TOGGLE_PIN   4 //GPIO to toggle on the RECEIVER

WiFiClient espClient;
PubSubClient client(espClient);

void callback(char* topic, byte* payload, unsigned int length);

void setup() {
  Serial.begin(SERIAL_BAUD);
  radioM.setupRadio();
  pinMode(LED, OUTPUT);

  pinMode(RX_TOGGLE_PIN, OUTPUT);

  gatewayM.setup_wifi();
  client.setServer(gatewayM.get_mqtttServer(), gatewayM.get_mqttPort());
  client.setCallback(callback);

  client.publish("iot", "Hello from ESP8266");
  client.subscribe("iot");
}
uint32_t packetCount = 0;

byte ackCount=0;
unsigned long lastDataSent = 0;
void loop(){
    if (radioM.getRadio().receiveDone())
    {
      String temperature = radioM.received();

      client.loop();
      client.publish("iot", "Temperature: ");
      client.subscribe("iot");
      client.publish("iot", (char*)radioM.getRadio().DATA);

      delay(5000);
    }

    if (radioM.getRadio().ACKRequested())
    {
      radioM.SendAcknowledgement();
      if (radioM.getAckCount()==0)
      {
        radioM.ackEffect();
      }
      printf("\n");
    }
}

void connect_to_mqtt_broker(){
  while (!client.connected()) {
    Serial.println("Connecting to MQTT...");

    if (client.connect("ESP8266Client", gatewayM.get_mqtttUser(), gatewayM.get_mqttPassword() )) {

      Serial.println("connected");

  } else {

      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);

    }
  }
}
void callback(char* topic, byte* payload, unsigned int length) {

  Serial.print("Message arrived in topic: ");
  Serial.println(topic);

  Serial.print("Message:");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }

  Serial.println();
  Serial.println("-----------------------");
}
