#include "radioManager.h"

RFM69 radioManager::getRadio(){
  return radio;
}

byte radioManager::getAckCount(){
  return ackCount;
}

void radioManager::setupRadio(){
  radio.initialize(FREQUENCY,NODEID,NETWORKID);
  radio.promiscuous(true);
  #ifdef IS_RFM69HW
    radio.setHighPower(); //only for RFM69HW!
  #endif
  radio.encrypt(ENCRYPTKEY);
  char buff[50];
  sprintf(buff, "\nListening at %d Mhz...", FREQUENCY==RF69_433MHZ ? 433 : FREQUENCY==RF69_868MHZ ? 868 : 915);
  Serial.println(buff);
  Serial.flush();
  radio.readAllRegs();
  Serial.println("End of radio setup");
}

String radioManager::received(){
  Serial.print("RECEIVER");
  sprintf(temperature, "%d",radio.DATA);
  Serial.print('[');Serial.print(radio.SENDERID);Serial.print("] ");
  Serial.print((char*)radio.DATA);
  Serial.print("   [RX_RSSI:");Serial.print(radio.RSSI);Serial.print("]");
  Serial.println();

  return temperature;
}

void radioManager::ackEffect(){
  Serial.print(" Pinging node ");
  Serial.print(theNodeID);
  Serial.print(" - ACK...");
  delay(3); //need this when sending right after reception .. ?
  if (radio.sendWithRetry(theNodeID, "ACK TEST", 8, 0))  // 0 = only 1 attempt, no retries
    Serial.print("ok!");
  else Serial.print("nothing");
  ackCount++;
}

void radioManager::SendAcknowledgement(){
  byte theNodeID = radio.SENDERID;
  radio.sendACK();
  Serial.print(" - ACK sent.");
}
