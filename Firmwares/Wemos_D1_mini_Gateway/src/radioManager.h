#ifndef RADIOMANAGER_H
#define RADIOMANAGER_H

#include <SPIFlash.h>
#include <SPI.h>
#include <RFM69.h>
#include <RFM69registers.h>
#include <RFM69_ATC.h>
#include <RFM69_OTA.h>

#define NETWORKID     100  //the same on all nodes that talk to each other
#define RECEIVER      1    //unique ID of the gateway/receiver
#define SENDER        2    // you could for example, have multiple senders
#define NODEID        RECEIVER  //change to "SENDER" if this is the sender node (the one with the button)
#define FREQUENCY     RF69_868MHZ
#define ENCRYPTKEY    "sampleEncryptKey" //exactly the same 16 characters/bytes on all nodes!
#define IS_RFM69HW    //uncomment only for RFM69HW! Remove/comment if you have RFM69W!

class radioManager{
  RFM69 radio;
  byte ackCount = 0;
  byte theNodeID;
  char temperature[12];

public:
    RFM69 getRadio();
    byte getAckCount();
    void setupRadio();
    String received();
    void SendAcknowledgement();
    void ackEffect();
};

#endif
