#ifndef BMPMANAGER_H
#define BMPMANAGER_H

#include <Wire.h>
#include <SPI.h>
#include <Adafruit_BMP280.h>
#include <Arduino.h>
#include "datastructures.h"

#define BMP_SCK  (13)
#define BMP_MISO (12)
#define BMP_MOSI (11)
#define BMP_CS   (10)


class bmpManager{

  Adafruit_BMP280 bmp; //I2C
public:

  Adafruit_BMP280 getBmp();
  void setup_bmp280();
  measurements getMeasurements();

};
#endif
