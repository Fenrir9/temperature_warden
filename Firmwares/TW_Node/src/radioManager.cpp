#include "radioManager.h"

RFM69 radioManager::getRFMradio(){
  return radio;
}

void radioManager::setupRadio(){
  radio.initialize(FREQUENCY,NODEID,NETWORKID);
  #ifdef IS_RFM69HW
    radio.setHighPower(); //only for RFM69HW!
  #endif
  radio.encrypt(ENCRYPTKEY);
  char buff[50];
  sprintf(buff, "\nListening at %d Mhz...", FREQUENCY==RF69_433MHZ ? 433 : FREQUENCY==RF69_868MHZ ? 868 : 915);
  Serial.println("Transmitter");
  Serial.println(buff);
  Serial.flush();
  radio.readAllRegs();
}

byte radioManager::received(byte ackC){
  //print message received to serial
  Serial.print('[');Serial.print(radio.SENDERID);Serial.print("] ");
  Serial.print((char)radio.DATA);
  Serial.print("   [RX_RSSI:");Serial.print(radio.RSSI);Serial.print("]");
  Serial.println();

  if (radio.ACKRequested())
{
  byte theNodeID = radio.SENDERID;
  radio.sendACK();
  Serial.print(" - ACK sent.");

  if (ackC++%3==0)
  {
    Serial.print(" Pinging node ");
    Serial.print(theNodeID);
    Serial.print(" - ACK...");
    delay(3); //need this when sending right after reception .. ?
    if (radio.sendWithRetry(theNodeID, "ACK TEST", 8, 0))  // 0 = only 1 attempt, no retries
      Serial.print("ok!");
    else Serial.print("nothing");
  }
}
return ackC;
}

void radioManager::sendTemperature(uint8_t tem){
  Serial.print("Temperature: ");
  char str[12];
  sprintf(str, "%d", tem);
  Serial.print(str);
  Serial.print("\n");
  radio.sendWithRetry(RECEIVER, str, 2);
}
