#ifndef DATASTRUCTURES_H
#define DATASTRUCTURES_H

#include <Arduino.h>

struct measurements{
  uint8_t temperature;
  float pressure;
  float approx_altitude;
};

#endif
