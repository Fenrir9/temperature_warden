#include <SPIFlash.h>
#include <Arduino.h>
#include <SPI.h>

#include "datastructures.h"
#include "bmpManager.h"
#include "radioManager.h"

#define SERIAL_BAUD   9600

//This part defines the LED pin and button pin
#define LED             9 //LED on D9
#define RX_TOGGLE_PIN   7 //GPIO to toggle on the RECEIVER

radioManager radioM;
bmpManager bmpM;

// the setup contains the start-up procedure and some useful serial data
void setup_bmp280();
void setup() {
  Serial.begin(SERIAL_BAUD);
  radioM.setupRadio();
  pinMode(LED, OUTPUT);
  pinMode(RX_TOGGLE_PIN, OUTPUT);
  Serial.println("End of setup");
  bmpM.setup_bmp280();
}

byte ackCount=0;

unsigned long lastDataSent = 0;
void loop(){
    if(lastDataSent + 5000 < millis()) {
        measurements m = bmpM.getMeasurements();
        radioM.sendTemperature(m.temperature);
        lastDataSent = millis();
    }
    if (radioM.getRFMradio().receiveDone())         //listening
    {
      ackCount = radioM.received(ackCount);

    }
}
