#include "bmpManager.h"

Adafruit_BMP280 bmpManager::getBmp(){
  return bmp;
}

void bmpManager::setup_bmp280() {

  if (!bmp.begin()) {
    Serial.println(F("Could not find a valid BMP280 sensor, check wiring!"));
    while (1);
  }

  bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
                  Adafruit_BMP280::SAMPLING_X2,     /* Temp. oversampling */
                  Adafruit_BMP280::SAMPLING_X16,    /* Pressure oversampling */
                  Adafruit_BMP280::FILTER_X16,      /* Filtering. */
                  Adafruit_BMP280::STANDBY_MS_500); /* Standby time. */
}

measurements bmpManager::getMeasurements(){
  measurements m;
  m.temperature = bmp.readTemperature();
  m.pressure = bmp.readPressure();
  m.approx_altitude = bmp.readAltitude(1013.25); /* Adjusted to local forecast! */

  return m;
}
